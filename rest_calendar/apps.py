from django.apps import AppConfig


class RestCalendarConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "src.rest_calendar"
    verbose_name = "Calendar"
    verbose_name_plural = "Calendars"
