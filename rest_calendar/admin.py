from django.contrib import admin
from nested_inline.admin import NestedModelAdmin, NestedStackedInline

from src.rest_calendar.models import (
    AvailabilityInterval,
    Schedule,
    ScheduleRule,
)


class AvailabilityIntervalInline(NestedStackedInline):
    model = AvailabilityInterval
    extra = 0
    fk_name = "rule"


class ScheduleRuleInline(NestedStackedInline):
    model = ScheduleRule
    inlines = (AvailabilityIntervalInline,)
    extra = 0
    fk_name = "schedule"


@admin.register(Schedule)
class ScheduleAdmin(NestedModelAdmin):
    inlines = (ScheduleRuleInline,)
