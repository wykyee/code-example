from datetime import date, timedelta

from django.contrib.auth import get_user_model
from drf_spectacular.utils import OpenApiParameter, extend_schema
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.mixins import (
    CreateModelMixin,
    DestroyModelMixin,
    ListModelMixin,
    RetrieveModelMixin,
)
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ModelViewSet

from src.core.services.timezone import activate_timezone
from src.core.validators import ValidationError, validate_request_date_range
from src.rest_calendar.filters import SlotFilter
from src.rest_calendar.models import Schedule, Slot
from src.rest_calendar.serializers import ScheduleSerializer, SlotSerializer
from src.rest_calendar.services.availability_interval import (
    get_date_intervals,
    get_wday_intervals,
)
from src.rest_calendar.services.slot import (
    get_available_slots,
    get_reserved_slots,
)
from src.rest_calendar.swagger import available_slots_responses
from src.service.models import Service


User = get_user_model()


class ScheduleViewSet(ModelViewSet):
    serializer_class = ScheduleSerializer
    http_method_names = ("get", "post", "put", "delete")

    def get_queryset(self):
        user = self.request.user
        return (
            Schedule.objects.select_related("user")
            .prefetch_related("rules", "rules__intervals")
            .filter(user=user)
        )

    @action(methods=["GET"], detail=True, url_path="set-as-default")
    def set_as_default(self, request: Request, **kwargs) -> Response:
        schedule = self.get_object()
        Schedule.objects.select_related("user").filter(
            user_id=schedule.user_id
        ).update(is_default=False)
        schedule.is_default = True
        schedule.save(update_fields=("is_default",))
        serializer = self.get_serializer(schedule)
        return Response(serializer.data)


class SlotViewSet(
    ListModelMixin,
    RetrieveModelMixin,
    DestroyModelMixin,
    CreateModelMixin,
    GenericViewSet,
):
    serializer_class = SlotSerializer
    queryset = Slot.objects.select_related("service", "service__user")
    filterset_class = SlotFilter

    @extend_schema(
        parameters=[
            OpenApiParameter(name="service", required=True, type=int),
            OpenApiParameter(name="range_start", required=True, type=date),
            OpenApiParameter(name="range_end", required=True, type=date),
            OpenApiParameter(
                name="timezone",
                required=False,
                type=str,
                description="List of available timezones: https://gist.github.com/heyalexej/8bf688fd67d7199be4a1682b3eec7568",
            ),
        ],
        responses=available_slots_responses,
    )
    @action(detail=False, url_path="available")
    def available_slots(self, request: Request) -> Response:
        service_id = request.query_params.get("service")
        activate_timezone(request.query_params.get("timezone", "UTC"))
        try:
            service = Service.objects.get(id=service_id)
        except Service.DoesNotExist:
            return Response(
                "service must contain correct id",
                status=status.HTTP_400_BAD_REQUEST,
            )

        try:
            start, end = validate_request_date_range(request)
        except ValidationError as e:
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)

        user_id = service.user_id
        response = dict(days=[])

        try:
            schedule = Schedule.objects.get(user_id=user_id, is_default=True)
        except Schedule.DoesNotExist:
            return Response(
                "User must have a default schedule",
                status=status.HTTP_400_BAD_REQUEST,
            )
        wdays = schedule.wdays
        exception_dates = schedule.exception_dates(start, end)

        # qs of unavailable intervals because of reservation
        reserved_slots = get_reserved_slots(user_id, start, end)
        date_availability_intervals = get_date_intervals(
            schedule, start, end
        ).values("rule__date", "start", "end")
        wday_availability_intervals = get_wday_intervals(schedule).values(
            "rule__wday", "start", "end"
        )

        for slots_date in (
            start + timedelta(days=n) for n in range((end - start).days + 1)
        ):
            response["days"].append({"date": slots_date, "slots": []})
            # firstly check if it's exception date in schedule
            if slots_date in exception_dates:
                response["days"][-1]["slots"] = get_available_slots(
                    slots_date,
                    date_availability_intervals,
                    service.duration,
                    reserved_slots,
                    "date",
                )
            # check if working day is available
            elif slots_date.strftime("%A").lower() in wdays:
                response["days"][-1]["slots"] = get_available_slots(
                    slots_date,
                    wday_availability_intervals,
                    service.duration,
                    reserved_slots,
                    "wday",
                )
            # or skip this date (zero available slots for this date)

        return Response(response)
