from datetime import datetime

from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import QuerySet


User = get_user_model()


class Schedule(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="schedules"
    )
    title = models.CharField(max_length=120)
    is_default = models.BooleanField(null=False, default=False)

    def __str__(self):
        return f"Schedule `{self.title}` for user {self.user.email}"

    @property
    def wdays(self) -> QuerySet:
        return self.rules.filter(wday__isnull=False).values_list(
            "wday", flat=True
        )

    def exception_dates(
        self, start: datetime.date, end: datetime.date
    ) -> QuerySet:
        return self.rules.filter(
            date__isnull=False, date__gte=start, date__lte=end
        ).values_list("date", flat=True)


class ScheduleRule(models.Model):
    class RuleType(models.TextChoices):
        wday = "wday"
        date = "date"

    class Wday(models.TextChoices):
        monday = "monday"
        tuesday = "tuesday"
        wednesday = "wednesday"
        thursday = "thursday"
        friday = "friday"
        saturday = "saturday"
        sunday = "sunday"

    schedule = models.ForeignKey(
        Schedule,
        on_delete=models.CASCADE,
        related_name="rules",
    )
    rule_type = models.CharField(max_length=4, choices=RuleType.choices)
    date = models.DateField(null=True, blank=True)
    wday = models.CharField(max_length=9, null=True, blank=True)


class AvailabilityInterval(models.Model):
    rule = models.ForeignKey(
        ScheduleRule, on_delete=models.CASCADE, related_name="intervals"
    )
    start = models.TimeField()
    end = models.TimeField()


class Slot(models.Model):
    service = models.ForeignKey(
        "service.Service", on_delete=models.CASCADE, related_name="slots"
    )
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()

    @property
    def user(self):
        return self.service.user

    @property
    def duration(self):
        return self.service.duration

    @property
    def date(self):
        return self.start_time.date()
