import datetime

from django.contrib.auth import get_user_model
from django.db import transaction
from rest_framework import serializers

from src.rest_calendar.models import (
    AvailabilityInterval,
    Schedule,
    ScheduleRule,
    Slot,
)
from src.rest_calendar.services.schedule import get_default_schedule
from src.rest_calendar.services.schedule_rule import (
    create_rules,
    validate_rules,
)
from src.service.models import Service
from src.service.serializers import ServiceSerializer
from src.users.serializers import UserSerializer


User = get_user_model()


class AvailabilityIntervalSerializer(serializers.ModelSerializer):
    class Meta:
        model = AvailabilityInterval
        exclude = ("id", "rule")

    def validate(self, data):
        if data["start"] >= data["end"]:
            raise serializers.ValidationError(
                "start time cant be less then end time"
            )
        return data


class ScheduleRuleSerializer(serializers.ModelSerializer):
    intervals = AvailabilityIntervalSerializer(many=True)
    date = serializers.DateField(required=False, allow_null=True)
    wday = serializers.ChoiceField(
        choices=ScheduleRule.Wday.choices, required=False, allow_null=True
    )
    rule_type = serializers.ChoiceField(choices=ScheduleRule.RuleType.choices)

    class Meta:
        model = ScheduleRule
        exclude = ("id", "schedule")

    def validate(self, data):
        rule_type = data["rule_type"]
        if rule_type == ScheduleRule.RuleType.wday and (
            data.get("date") or not data.get("wday")
        ):
            raise serializers.ValidationError(
                {
                    "rule_type": "Rule with type 'wday' must "
                    "have only wday inside"
                }
            )
        elif rule_type == ScheduleRule.RuleType.date and (
            data.get("wday") or not data.get("date")
        ):
            raise serializers.ValidationError(
                {
                    "rule_type": "Rule with type 'date' must "
                    "have only date inside"
                }
            )
        return data


class ScheduleSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    user_id = serializers.PrimaryKeyRelatedField(
        source="user", queryset=User.objects.all(), write_only=True
    )
    rules = ScheduleRuleSerializer(many=True)

    class Meta:
        model = Schedule
        fields = "__all__"
        read_only_fields = ("is_default",)

    def create(self, validated_data):
        rules = validated_data.pop("rules", [])
        if not validate_rules(rules):
            raise serializers.ValidationError(
                "Incorrect rules. Wdays or dates are not unique inside list."
            )
        with transaction.atomic():
            if not get_default_schedule(validated_data.get("user")):
                validated_data.update(is_default=True)
            instance = super().create(validated_data)
            create_rules(rules, instance)
        return instance

    def update(self, instance, validated_data):
        rules = validated_data.pop("rules", [])
        if not validate_rules(rules):
            raise serializers.ValidationError(
                "Incorrect rules. Wdays or dates are not unique inside list."
            )
        with transaction.atomic():
            instance = super().update(instance, validated_data)
            instance.rules.all().delete()
            create_rules(rules, instance)
        return instance


class SlotSerializer(serializers.ModelSerializer):
    service = ServiceSerializer(read_only=True)
    service_id = serializers.PrimaryKeyRelatedField(
        source="service", queryset=Service.objects.all(), write_only=True
    )
    end_time = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Slot
        fields = "__all__"

    def create(self, validated_data):
        validated_data.update(
            end_time=self.__get_end_time(
                validated_data["start_time"], validated_data["service"]
            )
        )
        return super().create(validated_data)

    @staticmethod
    def __get_end_time(
        start_time: datetime.datetime, service: Service
    ) -> datetime.datetime:
        return start_time + datetime.timedelta(minutes=service.duration)
