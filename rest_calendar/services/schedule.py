from typing import Optional

from django.contrib.auth import get_user_model

from src.rest_calendar.models import Schedule


User = get_user_model()


def get_default_schedule(user: User) -> Optional[Schedule]:
    try:
        return Schedule.objects.select_related("user").get(
            user=user, is_default=True
        )
    except Schedule.DoesNotExist:
        return
