from rest_framework import serializers

from src.rest_calendar.models import Schedule, ScheduleRule
from src.rest_calendar.services.availability_interval import (
    create_intervals,
    validate_intervals,
)


def validate_date_rules(rules: tuple[dict]) -> bool:
    unique_dates = {r["date"] for r in rules}
    if len(unique_dates) != len(rules):
        return False
    return True


def validate_wday_rules(rules: tuple[dict]) -> bool:
    unique_wdays = {r["wday"] for r in rules}
    if len(unique_wdays) != len(rules):
        return False
    return True


def validate_rules(rules: list[dict]) -> bool:
    wday_rules = tuple(
        filter(
            lambda rule: rule["rule_type"] == ScheduleRule.RuleType.wday, rules
        )
    )
    date_rules = tuple(
        filter(
            lambda rule: rule["rule_type"] == ScheduleRule.RuleType.date, rules
        )
    )
    if not validate_date_rules(date_rules) or not validate_wday_rules(
        wday_rules
    ):
        return False
    return True


def create_rules(rules: list, schedule: Schedule) -> None:
    for rule in rules:
        intervals = rule.pop("intervals")
        if not validate_intervals(intervals):
            raise serializers.ValidationError("Incorrect interval")
        rule_instance = ScheduleRule.objects.create(**rule, schedule=schedule)
        create_intervals(intervals, rule_instance)
