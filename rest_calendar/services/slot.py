import datetime as dt

from django.db.models import QuerySet
from django.utils.timezone import get_current_timezone, utc

from src.rest_calendar.models import Slot


def get_reserved_slots(
    user_id: int, start: dt.datetime, end: dt.datetime
) -> QuerySet:
    return Slot.objects.select_related("service").filter(
        start_time__date__gte=start,
        start_time__date__lte=end,
        service__user_id=user_id,
    )


def is_slot_reserved(
    reserved_slots: tuple,
    start,
    timedelta_service_duration: dt.timedelta,
) -> bool:
    """
    Checks if available slot isn't reserved
    (can be reserved even by other service)
    Bad scenario:
     - as - available slot
     - rs - reserved slot

    as(start)...rs(start)...as(end)...rs(end)
    rs(start)...as(start)...as(end)...rs(end)
    rs(start)...as(start)...rs(end)...as(end)
    as(start)...rs(start)...rs(end)...as(end)

    """
    return any(
        [
            any(
                [
                    start
                    <= reserved_slot.start_time
                    <= start + timedelta_service_duration,
                    start
                    <= reserved_slot.end_time
                    <= start + timedelta_service_duration,
                    start <= reserved_slot.start_time
                    and start + timedelta_service_duration
                    >= reserved_slot.end_time,
                    start >= reserved_slot.start_time
                    and start + timedelta_service_duration
                    <= reserved_slot.end_time,
                ]
            )
            for reserved_slot in reserved_slots
        ]
    )


def get_available_slots(
    date: dt.date,
    availability_intervals: QuerySet,
    service_duration: int,
    reserved_slots: QuerySet,
    mode: str,
) -> list:
    if mode == "wday":
        filter_lambda = (
            lambda i: i["rule__wday"] == date.strftime("%A").lower()
        )
    elif mode == "date":

        def filter_lambda(i):
            return i["rule__date"] == date

    else:
        return []

    # get tuple of available intervals for certain date
    intervals = tuple(filter(filter_lambda, availability_intervals))

    # get tuple of reserved slot for certain date
    date_reserved_slots = tuple(
        filter(lambda s: s.date == date, reserved_slots)
    )

    results = []
    for interval in intervals:
        start = dt.datetime.combine(date, interval["start"], tzinfo=utc)
        end = dt.datetime.combine(date, interval["end"], tzinfo=utc)
        td_service = dt.timedelta(minutes=service_duration)
        while True:
            is_reserved_slot = is_slot_reserved(
                date_reserved_slots, start, td_service
            )
            if start + td_service <= end:
                if not is_reserved_slot:
                    results.append(start.astimezone(get_current_timezone()))
                start += dt.timedelta(minutes=service_duration)
            else:
                break
    return results
