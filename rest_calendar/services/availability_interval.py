import datetime

from django.db.models import QuerySet

from src.rest_calendar.models import (
    AvailabilityInterval,
    Schedule,
    ScheduleRule,
)


def validate_intervals(intervals: list[dict]) -> bool:
    for i, interval in enumerate(intervals):
        after = i + 1
        other_intervals = tuple(intervals[:i] + intervals[after:])
        for other_interval in other_intervals:
            # Bad cases for intervals for the same date
            # other[start]...interval[start]...other[end]...interval[end]
            case1 = (
                other_interval["start"]
                <= interval["start"]
                <= other_interval["end"]
            )
            # interval[start]...other[start]...interval[end]...other[end]
            case2 = (
                other_interval["end"]
                >= interval["end"]
                >= other_interval["start"]
            )
            # interval[start]...other[start]...other[end]...interval[end]
            case3 = (
                interval["start"] <= other_interval["start"]
                and interval["end"] >= other_interval["end"]
            )
            # other[start]...interval[start]...interval[end]...other[end]
            case4 = (
                interval["start"] >= other_interval["start"]
                and interval["end"] <= other_interval["end"]
            )
            if any([case1, case2, case3, case4]):
                return False
    return True


def create_intervals(intervals: dict, rule: ScheduleRule) -> None:
    AvailabilityInterval.objects.bulk_create(
        AvailabilityInterval(**interval, rule=rule) for interval in intervals
    )


def get_wday_intervals(schedule: Schedule) -> QuerySet:
    return AvailabilityInterval.objects.select_related(
        "rule__schedule", "rule"
    ).filter(rule__schedule=schedule, rule__wday__isnull=False)


def get_date_intervals(
    schedule: Schedule, start: datetime.date, end: datetime.date
) -> QuerySet:
    return AvailabilityInterval.objects.select_related(
        "rule__schedule"
    ).filter(
        rule__schedule=schedule,
        rule__date__gte=start,
        rule__date__lte=end,
    )
