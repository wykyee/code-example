# Generated by Django 3.2.9 on 2021-12-29 14:26

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.BigAutoField(auto_created=True,
                 primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=120)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE,
                 related_name='schedules', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ScheduleRule',
            fields=[
                ('id', models.BigAutoField(auto_created=True,
                 primary_key=True, serialize=False, verbose_name='ID')),
                ('rule_type', models.CharField(choices=[
                 ('wday', 'Wday'), ('date', 'Date')], max_length=4)),
                ('date', models.DateField(null=True)),
                ('wday', models.CharField(blank=True, max_length=9, null=True)),
                ('schedule', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE,
                 related_name='rules', to='rest_calendar.schedule')),
            ],
        ),
        migrations.CreateModel(
            name='AvailabilityInterval',
            fields=[
                ('id', models.BigAutoField(auto_created=True,
                 primary_key=True, serialize=False, verbose_name='ID')),
                ('time_from', models.TimeField()),
                ('time_to', models.TimeField()),
                ('rule', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE,
                 related_name='intervals', to='rest_calendar.schedulerule')),
            ],
        ),
    ]
