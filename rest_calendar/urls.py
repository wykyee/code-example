from rest_framework.routers import SimpleRouter

from src.rest_calendar.views import ScheduleViewSet, SlotViewSet


router = SimpleRouter()
router.register("schedules", ScheduleViewSet, basename="schedule")
router.register("slots", SlotViewSet, basename="slot")

urlpatterns = router.urls
