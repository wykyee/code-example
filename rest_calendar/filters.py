from django_filters import rest_framework as filters

from src.rest_calendar.models import Slot


class SlotFilter(filters.FilterSet):
    user = filters.NumberFilter(field_name="service__user", required=True)

    class Meta:
        model = Slot
        fields = ("user",)
