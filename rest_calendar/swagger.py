from rest_framework import serializers


class AvailableSlotsDate(serializers.Serializer):
    date = serializers.DateField()
    slots = serializers.ListSerializer(child=serializers.DateTimeField())


class AvailableSlots(serializers.Serializer):
    days = AvailableSlotsDate(many=True)


available_slots_responses = {200: AvailableSlots}
