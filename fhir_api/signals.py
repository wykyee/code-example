from django.conf import settings

from src.fhir_api.tasks import send_cloud_request_task


def send_fhir_signal(instance, created: bool, **kwargs):
    if settings.GOOGLE_CLOUD_ENABLED and instance.can_be_cloud_saved:
        params = {
            "method": "POST",
            "cloud_endpoint": instance.cloud_endpoint,
            "body": instance.fhir_data,
            "update_uuid_data": {
                "app": instance._meta.app_label,
                "model": instance._meta.model_name,
                "pk": instance.pk,
            },
        }
        if instance.uuid:
            params["method"] = "PUT"

        send_cloud_request_task.apply_async(kwargs=params)
