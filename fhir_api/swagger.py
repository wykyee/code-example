from drf_yasg import openapi


cloud_fhir_response = {
    "200": openapi.Response(
        description="Successful response",
        examples={
            "application/json": {"id": "1", "cloud_data": {"field": "value"}}
        },
    )
}

multiple_fhir_response_schema = openapi.Schema(
    type=openapi.TYPE_ARRAY,
    items=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        title="Fhir data",
        description="Cloud response",
    ),
)

ids_request_body = openapi.Schema(
    type=openapi.TYPE_ARRAY,
    items=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={"id": openapi.Schema(type=openapi.TYPE_STRING)},
    ),
)

codeable_concept_schema = {
    "coding": openapi.Schema(
        type=openapi.TYPE_ARRAY,
        items=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                "system": openapi.Schema(type=openapi.TYPE_STRING),
                "code": openapi.Schema(type=openapi.TYPE_STRING),
                "display": openapi.Schema(type=openapi.TYPE_STRING),
            },
        ),
    ),
    "text": openapi.Schema(type=openapi.TYPE_STRING),
}

period_schema = {
    "start": openapi.Schema(
        type=openapi.TYPE_STRING, format=openapi.FORMAT_DATETIME
    ),
    "end": openapi.Schema(
        type=openapi.TYPE_STRING, format=openapi.FORMAT_DATETIME
    ),
}

dosage_schema = {
    "route": openapi.Schema(
        tytle="CodeableConcept",
        type=openapi.TYPE_OBJECT,
        properties=codeable_concept_schema,
    ),
    "site": openapi.Schema(
        tytle="CodeableConcept",
        type=openapi.TYPE_OBJECT,
        properties=codeable_concept_schema,
    ),
    "method": openapi.Schema(
        tytle="CodeableConcept",
        type=openapi.TYPE_OBJECT,
        properties=codeable_concept_schema,
    ),
    "additionalInstruction": openapi.Schema(
        type=openapi.TYPE_ARRAY,
        items=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            title="CodeableConcept",
            properties=codeable_concept_schema,
        ),
    ),
    "patientInstruction": openapi.Schema(type=openapi.TYPE_STRING),
    "timing": openapi.Schema(tytle="Timing", type=openapi.TYPE_OBJECT),
    "doseAndRate": openapi.Schema(
        type=openapi.TYPE_ARRAY,
        items=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                "doseQuantity": openapi.Schema(
                    type=openapi.TYPE_OBJECT,
                    properties={
                        "value": openapi.Schema(type=openapi.TYPE_NUMBER),
                        "unit": openapi.Schema(type=openapi.TYPE_STRING),
                        "system": openapi.Schema(type=openapi.TYPE_STRING),
                        "code": openapi.Schema(type=openapi.TYPE_STRING),
                    },
                ),
            },
        ),
    ),
}

quantity_schema = {
    "value": openapi.Schema(type=openapi.TYPE_STRING),
    "comparator": openapi.Schema(type=openapi.TYPE_STRING),
    "unit": openapi.Schema(type=openapi.TYPE_STRING),
    "system": openapi.Schema(type=openapi.TYPE_STRING),
    "code": openapi.Schema(type=openapi.TYPE_STRING),
}

reference_schema = {
    "reference": openapi.Schema(type=openapi.TYPE_STRING),
    "type": openapi.Schema(type=openapi.TYPE_STRING),
    "display": openapi.Schema(type=openapi.TYPE_STRING),
}

annotation_schema = {
    "time": openapi.Schema(
        type=openapi.TYPE_STRING, format=openapi.FORMAT_DATETIME
    ),
    "text": openapi.Schema(type=openapi.TYPE_STRING),
}
