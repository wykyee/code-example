from rest_framework import serializers

from src.fhir_api.services import make_fhir_coding, make_fhir_reference


class FhirCodeableConceptField(serializers.Field):
    """
    Field uses only in case of 1 dict in coding list, because
    often only 1 dict is used.
    Otherwise use custom SerializerMethodField with make_fhir_coding.
    """

    def __init__(self, *args, **kwargs):
        self.system = kwargs.pop("system")
        self.display = kwargs.pop("display", None)
        self.text = kwargs.pop("text", None)
        if "read_only" not in kwargs:
            kwargs["read_only"] = True
        super().__init__(*args, **kwargs)

    def to_internal_value(self, data):
        return data

    def to_representation(self, value):
        if value or self.display:
            coding = {
                "system": self.system,
                "code": value,
                "display": self.display,
            }
            return make_fhir_coding(coding, text=self.text)


class FhirReferenceField(serializers.Field):
    """
    Serializer's field for FHIR Reference DataType.
    """

    def __init__(self, *args, **kwargs):
        self.identifier = kwargs.pop("identifier", None)
        self.display = kwargs.pop("display", None)
        self.type_ = kwargs.pop("type_", None)
        if "read_only" not in kwargs:
            kwargs["read_only"] = True
        super().__init__(*args, **kwargs)

    def to_internal_value(self, data):
        return data

    def to_representation(self, value):
        if value:
            return make_fhir_reference(
                reference=value,
                type_=self.type_,
                identifier=self.identifier,
                display=self.display,
            )


class FhirPeriodField(serializers.Serializer):
    start = serializers.DateTimeField(required=False)
    end = serializers.DateTimeField(required=False)

    def validate_start(self, value):
        return value.isoformat()

    def validate_end(self, value):
        return value.isoformat()


class FhirAnnotationField(serializers.Serializer):
    time = serializers.DateTimeField(required=False)
    text = serializers.CharField()

    def validate_time(self, value):
        return value.isoformat()
