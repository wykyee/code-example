from django.utils.translation import gettext_lazy as _
from rest_framework import status
from rest_framework.exceptions import APIException


class GoogleCloudApiError(Exception):
    pass


class FhirValidationError(Exception):
    pass


class IncorrectTaskKwargsError(Exception):
    pass


class CloudActionError(APIException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = _(
        "Resource is not found. Can't interact with Google Cloud"
    )
    default_code = "cloud_error"
