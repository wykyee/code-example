from django.db import models


class CodeableConcept(models.Model):
    system = models.CharField(max_length=100)
    code = models.CharField(max_length=100)
    display = models.CharField(max_length=100)

    class Meta:
        abstract = True
