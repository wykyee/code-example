from django.conf import settings
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from src.fhir_api.exceptions import GoogleCloudApiError
from src.fhir_api.google_cloud_api import cloud_service
from src.fhir_api.swagger import cloud_fhir_response


class CloudViewSet(GenericViewSet):
    patient = None
    pagination_class = None
    filter_backends = ()
    lookup_field = "uuid"

    def dispatch(self, request, *args, **kwargs):
        if not settings.GOOGLE_CLOUD_ENABLED:
            raise GoogleCloudApiError
        return super().dispatch(request, *args, **kwargs)

    def list(self, url: str, request, *args, **kwargs):
        try:
            cloud_data = cloud_service.get(url)
        except GoogleCloudApiError as e:
            return Response(
                {"error": str(e)}, status=status.HTTP_400_BAD_REQUEST
            )
        return Response(cloud_data)

    @swagger_auto_schema(responses=cloud_fhir_response)
    def create(self, data_to_save: dict, url: str, request, *args, **kwargs):
        self._update_request_data(request)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        data = serializer.validated_data
        try:
            cloud_data = cloud_service.post(url, data)
        except GoogleCloudApiError as e:
            return Response(
                {"error": str(e)}, status=status.HTTP_400_BAD_REQUEST
            )
        instance = serializer.save(uuid=cloud_data["id"], **data_to_save)
        response = {
            "id": instance.id,
            "cloud_data": cloud_data,
        }
        return Response(response, status=status.HTTP_201_CREATED)

    @swagger_auto_schema(responses=cloud_fhir_response)
    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            cloud_data = cloud_service.get(instance.cloud_endpoint)
        except GoogleCloudApiError as e:
            return Response(
                {"error": str(e)}, status=status.HTTP_400_BAD_REQUEST
            )
        response = {"id": instance.id, "cloud_data": cloud_data}
        return Response(response)

    @swagger_auto_schema(responses=cloud_fhir_response)
    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        self._update_request_data(request)
        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)

        url = instance.cloud_endpoint
        data = serializer.validated_data
        data["id"] = str(instance.uuid)
        try:
            cloud_data = cloud_service.put(url, data)
        except GoogleCloudApiError as e:
            return Response(
                {"error": str(e)}, status=status.HTTP_400_BAD_REQUEST
            )

        serializer.save()
        response = {
            "id": instance.id,
            "cloud_data": cloud_data,
        }
        return Response(response, status=status.HTTP_201_CREATED)

    def destroy(self, request, *args, body=None, to_delete=True, **kwargs):
        instance = self.get_object()
        url = instance.cloud_endpoint
        try:
            if not to_delete:
                cloud_service.patch(url, body or {})
            else:
                cloud_service.delete(url)
        except GoogleCloudApiError as e:
            return Response(
                {"error": str(e)}, status=status.HTTP_400_BAD_REQUEST
            )
        return Response(status=status.HTTP_204_NO_CONTENT)

    def _update_request_data(self, request):
        raise NotImplementedError


class CloudFhirViewSet(GenericViewSet):
    fhir_deserializer_class = None

    def list(self, url: str, request, *args, **kwargs):
        try:
            cloud_data = cloud_service.get(url)
        except GoogleCloudApiError as e:
            return Response(
                {"error": str(e)}, status=status.HTTP_400_BAD_REQUEST
            )
        entry = cloud_data.get("entry")
        if entry:
            data = self.fhir_deserializer_class(data=entry, many=True).data
        else:
            data = []
        return Response(data)
