from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response

from src.fhir_api.exceptions import FhirValidationError


class FhirActionMixin:
    @action(detail=True, methods=["GET"])
    def fhir(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            response_data = instance.fhir_data
            return Response(response_data)
        except FhirValidationError as error:
            return Response(
                {"error": str(error)}, status=status.HTTP_400_BAD_REQUEST
            )
