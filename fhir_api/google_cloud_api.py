from typing import Optional

from django.conf import settings
from google.auth.transport import requests
from google.oauth2 import service_account
from requests import HTTPError

from src.fhir_api.exceptions import GoogleCloudApiError


class GoogleCloudHealthcareApi:
    # https://github.com/GoogleCloudPlatform/python-docs-samples/blob/master/healthcare/api-client/v1/fhir/fhir_resources.py
    _BASE_URL = "https://healthcare.googleapis.com/v1"
    _DEFAULT_HEADERS = {"Content-Type": "application/fhir+json;charset=utf-8"}

    def __init__(self):
        self.url = "{}/projects/{}/locations".format(
            self._BASE_URL, settings.GOOGLE_CLOUD["PROJECT_ID"]
        )
        self.session = None
        self.session_methods = dict()

    def set_session(self) -> None:
        """
        Creates an authorized Requests Session.
        """
        credentials = service_account.Credentials.from_service_account_file(
            settings.GOOGLE_CLOUD["CREDENTIALS_PATH"]
        )
        scoped_credentials = credentials.with_scopes(
            ["https://www.googleapis.com/auth/cloud-platform"]
        )
        self.session = requests.AuthorizedSession(scoped_credentials)
        self.session_methods = {
            "GET": self.session.get,
            "POST": self.session.post,
            "PUT": self.session.put,
            "PATCH": self.session.patch,
            "DELETE": self.session.delete,
        }

    def get_fhir_resource_path(
        self,
        location: str,
        dataset_id: str,
        fhir_store_id: str,
        resource_path: Optional[str] = None,
    ) -> str:
        """
        Returns full path to resource in Google Cloud.
        Requires dataset_id, that stores in CompanyAccount,
        fhir_store_id - practice uuid, and path to the resource
        like `Patient` or `Patient/<uuid>`
        """
        return "{}/{}/datasets/{}/fhirStores/{}/fhir/{}".format(
            self.url, location, dataset_id, fhir_store_id, resource_path
        )

    def get_dataset_path(
        self, location: str, dataset_id: str, for_create: bool = False
    ) -> str:
        # https://cloud.google.com/healthcare/docs/how-tos/datasets#healthcare-create-dataset-cli-curl
        path = f"{self.url}/{location}/datasets"
        path += f"?datasetId={dataset_id}" if for_create else f"/{dataset_id}"
        return path

    def get_fhir_store_path(
        self,
        location: str,
        dataset_id: str,
        fhir_store_id: str,
        for_create: bool = False,
    ) -> str:
        # https://cloud.google.com/healthcare/docs/how-tos/fhir#api
        path = f"{self.url}/{location}/datasets/{dataset_id}/fhirStores"
        path += (
            f"?fhirStoreId={fhir_store_id}"
            if for_create
            else f"/{fhir_store_id}"
        )
        return path

    def _do_request(
        self,
        method: str,
        url: str,
        body: dict = None,
        headers: dict = None,
    ) -> dict:
        """
        method: GET/POST/PUT/DELETE
        url: url for dataset or fhir_store or resource
        body: request_body for POST/PUT
        """
        if not headers:
            headers = self._DEFAULT_HEADERS
        try:
            response = self.session_methods[method](
                url=url, json=body, headers=headers
            )
            response.raise_for_status()
        except (ValueError, HTTPError) as e:
            raise GoogleCloudApiError(e)
        resource = response.json()
        return resource

    def post(self, url: str, body: dict):
        return self._do_request(method="POST", url=url, body=body)

    def get(self, url: str):
        return self._do_request(method="GET", url=url)

    def put(self, url: str, body: dict):
        return self._do_request(method="PUT", url=url, body=body)

    def patch(self, url: str, body: dict):
        headers = {"Content-Type": "application/json-patch+json"}
        return self._do_request(
            method="PATCH", url=url, body=body, headers=headers
        )

    def delete(self, url: str):
        return self._do_request(method="DELETE", url=url)


cloud_service = GoogleCloudHealthcareApi()
if settings.GOOGLE_CLOUD_ENABLED:
    cloud_service.set_session()
