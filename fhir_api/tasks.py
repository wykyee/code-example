import logging
from typing import Optional

from celery import Task, states
from celery.exceptions import Ignore
from django.apps import apps

from config.celery import app
from src.fhir_api.exceptions import (
    GoogleCloudApiError,
    IncorrectTaskKwargsError,
)
from src.fhir_api.google_cloud_api import cloud_service


logger = logging.getLogger(__name__)

METHODS = {
    "GET": cloud_service.get,
    "POST": cloud_service.post,
    "PUT": cloud_service.put,
    "PATCH": cloud_service.patch,
    "DELETE": cloud_service.delete,
}


@app.task(bind=True)
def send_cloud_request_task(
    self: Task,
    cloud_endpoint: str,
    method: str,
    body: Optional[dict] = None,
    update_uuid_data: Optional[dict] = None,
) -> None:
    """
    Async sends request to Google Cloud Platform.
    `update_uuid_data` - requires app_label, model_name and pk of object
    """
    if not body:
        body = dict()

    instance = None
    if update_uuid_data:
        app_label = update_uuid_data.get("app")
        model_name = update_uuid_data.get("model")
        pk = update_uuid_data.get("pk")
        if app_label and model_name and pk:
            try:
                model = apps.get_model(app_label, model_name)
            except (ValueError, LookupError) as e:
                raise IncorrectTaskKwargsError(e)

            instance = model.objects.filter(pk=pk)
            if not instance.exists():
                raise IncorrectTaskKwargsError
    try:
        if method not in ["GET", "DELETE"]:
            response = METHODS[method](cloud_endpoint, body)
        else:
            response = METHODS[method](cloud_endpoint)

        if instance:
            instance.update(uuid=response["id"])
    except GoogleCloudApiError as e:
        logger.error(str(e))
        self.update_state(state=states.FAILURE)
        raise Ignore()


@app.task(bind=True)
def create_practice_cloud_task(
    self: Task,
    dataset: str,
    fhir_store: str,
    practice_pk: int,
    cloud_region: str,
) -> None:
    """
    Creates datastore with fhir_store for practice
    """

    dataset_path = cloud_service.get_dataset_path(
        cloud_region, dataset, for_create=True
    )
    fhir_store_path = cloud_service.get_fhir_store_path(
        cloud_region,
        dataset,
        fhir_store,
        for_create=True,
    )

    try:
        # creating dataset
        cloud_service.post(dataset_path, {})
        # creating fhir_store
        cloud_service.post(
            fhir_store_path, {"version": "R4", "enableUpdateCreate": True}
        )
    except GoogleCloudApiError as e:
        logger.error(str(e))
        self.update_state(state=states.FAILURE)
        raise Ignore()
    apps.get_model("companies", "Practice").objects.filter(
        pk=practice_pk
    ).update(
        cloud_status=states.SUCCESS, dataset=dataset, fhir_store=fhir_store
    )
