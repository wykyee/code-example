from django.apps import AppConfig
from django.db.models.signals import post_save

from src.fhir_api.signals import send_fhir_signal


class FhirApiConfig(AppConfig):
    name = "src.fhir_api"

    def ready(self):
        from src.patients.models import Patient

        post_save.connect(send_fhir_signal, sender=Patient)
