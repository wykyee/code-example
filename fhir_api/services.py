from typing import Optional, Tuple

from src.fhir_api.exceptions import FhirValidationError


def make_fhir_coding(*args: dict, text: Optional[str] = None) -> dict:
    """
    https://www.hl7.org/fhir/datatypes.html#CodeableConcept
    arg example:
        {"system": `system`, "code": `code`, "display": `display`}
    returns CodeableConcept fhir dictionary.
    """
    data = dict(coding=[])
    for coding in args:
        if not coding.get("system"):
            raise FhirValidationError()

        non_blank_fields = ("display", "code")
        for field in non_blank_fields:
            if coding.get(field) == "":
                del coding[field]

        data["coding"].append(coding)

    if text:
        data["text"] = text
    return data


def make_fhir_reference(
    reference: str,
    type_: Optional[str] = None,
    identifier: Optional[str] = None,
    display: Optional[str] = None,
) -> dict:
    """
    https://www.hl7.org/fhir/references.html#Reference
    """
    if not isinstance(reference, str):
        raise FhirValidationError("Reference must be string")
    data = dict(reference=reference)
    if type_ and isinstance(type_, str):
        data["type"] = type_
    if identifier and isinstance(identifier, str):
        data["identifier"] = identifier
    if display and isinstance(display, str):
        data["display"] = display

    return data
